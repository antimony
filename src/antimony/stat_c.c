#include "antimony.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

sb_word_t sbJ_stat(sb_word_t path) {
  const char *cpath = sbJ_cstring_from_blob(path);
  struct stat *st = (struct stat*) sbJ_allocate_bytes(sizeof(struct stat));
  stat(cpath, st);
  return (sb_word_t) st;
}

sb_word_t sbJ_stat_isdir(sb_word_t stat_data) {
  return S_ISDIR(((struct stat*) stat_data)->st_mode) ?
    sbJ_true : sbJ_false;
}
