section functions
export chdir closedir copy mkdir opendir path_basename path_join path_split \
  rmdir unlink unlink_recursive
import eq false ge gt le lt ne true
import core.chdir core.closedir core.mkdir core.opendir core.rmdir core.unlink
import allocate_bytes blob_find_last_before blob_length close_stream \
  compare_blobs concatenate_blobs copy_blob_part cstring_from_blob \
  errno_error error input_file make_blob readdir output_file \
  read_bytes_from_stream split_blob stat stat_isdir \
  write_bytes_to_stream

function chdir new_dir {
  # Changes the current working directory to new_dir.
  core.call core.chdir (cstring_from_blob new_dir)
  return 0
}

function closedir handle {
  # Closes a directory handle.
  return (core.call core.closedir handle)
}

function copy source dest {
  # Copies source file to dest.
  var infile = input_file source
  var outfile = output_file dest
  loop {
    var size = 16384
    var buffer = allocate_bytes size
    var n = 0
    var m = 0
    do
    set n read_bytes_from_stream infile buffer size
    while (gt n 0)
    set m write_bytes_to_stream outfile buffer n
    if (ne n m) {
      error "I/O error writing file"
    }
    while (eq n size)
  }
  close_stream infile
  close_stream outfile
  return 0
}

function mkdir path mode {
  # Creates a directory at path, with permissions given by mode.
  let result core.call core.mkdir (cstring_from_blob path) mode
  if (eq result -1) {
    errno_error
  }
  return result
}

function path_basename path {
  # Returns the part of path after the last directory separator.
  # If no directory separator is present, returns path.
  # As a special case, if path is "/", returns path.
  var end = blob_length path
  var start = blob_find_last_before path 47 end
  loop {
    do
    while (ne start -1)
    while (eq start (sub end 1))
    set end start
    set start blob_find_last_before path 47 end
  }
  if (lt start 1) {
    if (eq end (blob_length path)) {
      return path
    }
    if (eq end 0) {
      return "/"
    }
  }
  return (copy_blob_part path start (sub end start))
}

function path_join first second {
  let prefix concatenate_blobs first "/"
  return (concatenate_blobs prefix second)
}

function path_split path {
  # Returns the components of path.
  return (split_blob path 47)	# /
}

function opendir path {
  # Opens a directory and returns a handle to it.
  let handle core.call core.opendir (cstring_from_blob path)
  if (eq handle 0) {
    errno_error
  }
  return handle
}

function rmdir path {
  # Removes directory path. Only the directory itself is removed,
  # not its contents, so this will fail if the directory is not empty.
  # To remove a directory and all its contents, use unlink_recursive.
  return (core.call core.rmdir (cstring_from_blob path))
}

function unlink path {
  # Unlinks a file.
  return (core.call core.unlink (cstring_from_blob path))
}

function unlink_recursive path {
  # Unlinks a path. If the path designates a directory, its contents
  # are removed, first.
  let st stat path
  if (stat_isdir st) {
    # Recursively remove all directory entries.
    var dir = opendir path
    var dot = "."
    var dotdot = ".."
    loop {
      var name = 0
      do
      set name readdir dir
      while (ne name -1)
      if (eq (compare_blobs name dot) 0) {
        # skip
      } else if (eq (compare_blobs name dotdot) 0) {
        # skip
      } else {
        unlink_recursive (path_join path name)
      }
    }
    closedir dir
    return (rmdir path)
  } else {
    return (unlink path)
  }
}
