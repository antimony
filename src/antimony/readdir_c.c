#include "antimony.h"
#include <dirent.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

sb_word_t sbJ_readdir(DIR *dirp) {
  errno = 0;
  struct dirent *ent = readdir(dirp);
  if (errno != 0) {
    error(sbJ_blob_from_cstring(strerror(errno)));
  }
  if (ent == NULL) {
    return SB_NIL;
  }
  return sbJ_blob_from_cstring(ent->d_name);
}
