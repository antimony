#include "antimony.h"
#include <errno.h>

sb_word_t sbJ_errno() {
  return (sb_word_t) errno;
}
