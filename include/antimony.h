#ifndef ANTIMONY_H
#define ANTIMONY_H

#include <stdint.h>

typedef uint8_t sb_byte_t;
typedef intptr_t sb_word_t;

#define SB_NIL ((sb_word_t) -1)

sb_word_t sbJ_false;
sb_word_t sbJ_true;

sb_byte_t *sbJ_allocate_bytes(sb_word_t nwords);
sb_word_t *sbJ_allocate_words(sb_word_t nwords);
sb_word_t sbJ_blob_from_cstring(const char *str);
char *sbJ_cstring_from_blob(sb_word_t blob);
void sbJ_error(sb_word_t message);
sb_word_t sbJ_make_blob(sb_byte_t *bytes, sb_word_t length);

#endif /* ndef ANTIMONY_H */
